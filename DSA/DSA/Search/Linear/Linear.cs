﻿using System;
using DSA.Print;
namespace DSA.Search
{
    public class Linear
    {
        public Linear()
        {
        }

		public string LinearSearch(int[] searchArray, int[] findNumbers)
        {
            try
			{
				int[] numberFound = new int[findNumbers.Length];
				for (int number = 0; number < findNumbers.Length;number++)
                {
                    for (int i = 0; i < searchArray.Length; i++)
                    {
						if (findNumbers[number] == searchArray[i])
                        {
							numberFound[number] = findNumbers[number];
                        }
                    }
                }
				"Linear Search:".PrintOutput(numberFound);
                return numberFound.ToString();
            }
            catch (Exception ex)
            {
				ex.PrintException();
                throw;
            }
        }
    }
}
