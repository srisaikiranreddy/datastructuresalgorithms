﻿using System;
using DSA.Data;
using DSA.Print;

namespace DSA
{
    public class Program
    {
        public static void Main(string[] args)
        {
			try
            {
				//Console.Write("")
				//Console.Read();
				Data.Data.NegativePositiveRandomNumbers().PrintArray();
				Data.Data.NegativePositiveRepeatingRandomNumbers().PrintArray();
            }
            catch (Exception ex)
            {
				Console.WriteLine("Error Message:{0},Error StackTrace:{1}", ex.Message, ex.StackTrace);
            }
        }
    }
}
