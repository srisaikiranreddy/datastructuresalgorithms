﻿using System;
namespace DSA.Data
{
    public static class Data
    {
		public static int[] PositiveRandomNumbers()
		{
			return new int[] { 14, 33, 27, 10, 35, 19, 42, 44 };
		}
		public static int[] NegativeRandomNumbers()
        {
			return new int[] {-14, -33, -27, -10, -35, -19, -42, -44 };
        }
		public static int[] NegativePositiveRandomNumbers()
		{
			return new int[] { -14, 33, -27, 10, 35, -19, 42, 44 };
		}
		public static int[] PositiveRepeatingRandomNumbers()
        {
			return new int[] { 14, 33, 27, 10, 35, 19, 42, 44, 10, 35, 19, 33, 12, 1, 15, 16 };
        }
		public static int[] NegativeRepeatingRandomNumbers()
        {
			return new int[] { -14, -33, -27, -10, -35, -19, -42, -44, -10, -35, -19, -33, -12, -1, -15, 16 };
        }
		public static int[] NegativePositiveRepeatingRandomNumbers()
        {
			return new int[] { -14, -33, 27, -10, 35, -19, 42, 44, -10, 35, 19, -33, 12, 1, 15, 16 };
        }
    }
}
