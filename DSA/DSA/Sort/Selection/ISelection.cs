﻿using System;
namespace DSA.Sort
{
    public interface ISelection
    {
		int[] SelectionSort(int[] unSortedArray);
    }
}
