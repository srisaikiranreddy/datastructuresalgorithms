﻿using System;
using DSA.Print;
namespace DSA.Sort
{
	public class Selection:ISelection
    {
        public Selection()
        {
        }

		//Step 1 − Set MIN to location 0
        //Step 2 − Search the minimum element in the list
        //Step 3 − Swap with value at location MIN
        //Step 4 − Increment MIN to point to next element
        //Step 5 − Repeat until list is sorted
        public int[] SelectionSort(int[] unSortedArray)
        {
            try
            {
                int minimumPosition = 0;
                for (int write = 0; write < unSortedArray.Length; write++)
                {
                    minimumPosition = write;
                    for (int sort = write + 1; sort < unSortedArray.Length; sort++)
                    {
                        if (unSortedArray[minimumPosition] > unSortedArray[sort])
                        {
                            int temp = unSortedArray[minimumPosition];
                            unSortedArray[minimumPosition] = unSortedArray[sort];
                            unSortedArray[sort] = temp;
                        }
                    }
					write.PrintIteration(unSortedArray);
                }
				"Selection Final".PrintOutput(unSortedArray);
                return unSortedArray;
            }
            catch (Exception ex)
            {
				ex.PrintException();
                throw;
            }
        }
    }
}
