﻿using System;
using DSA.Print;
namespace DSA.Sort
{
	public class Bubble:IBubble
    {
        public Bubble()
        {
        }

		public int[] BubbleSort(int[] unSortedArray)
        {
            try
            {
                for (int write = 0; write < unSortedArray.Length; write++)
                {
                    for (int sort = write + 1; sort < unSortedArray.Length; sort++)
                    {
                        if (unSortedArray[write] > unSortedArray[sort])
                        {
                            int temp = unSortedArray[write];
                            unSortedArray[write] = unSortedArray[sort];
                            unSortedArray[sort] = temp;
                        }
                    }
					write.PrintIteration(unSortedArray);
                }
				"BubbleSort Final".PrintOutput(unSortedArray); 
                return unSortedArray;
            }
            catch (Exception ex)
            {
				ex.PrintException();
                throw;
            }
        }
    }
}
