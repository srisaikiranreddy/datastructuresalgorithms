﻿using System;
namespace DSA.Sort
{
	public interface IBubble
	{
		int[] BubbleSort(int[] unSortedArray);
    }
}
