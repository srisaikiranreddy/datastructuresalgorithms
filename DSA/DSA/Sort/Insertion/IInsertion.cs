﻿using System;
namespace DSA.Sort
{
    public interface IInsertion
    {
		int[] InsertionSort(int[] unSortedArray);
    }
}
