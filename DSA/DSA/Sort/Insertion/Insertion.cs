﻿using System;
using DSA.Print;
namespace DSA.Sort
{
	public class Insertion:IInsertion
    {
        public Insertion()
        {
        }

		//Step 1 − If it is the first element, it is already sorted. return 1;
        //Step 2 − Pick next element
        //Step 3 − Compare with all elements in the sorted sub-list
        //Step 4 − Shift all the elements in the sorted sub-list that is greater than the value to be sorted
        //Step 5 − Insert the value
        //Step 6 − Repeat until list is sorted
        public int[] InsertionSort(int[] unSortedArray)
        {
            try
            {
                int holePosition = 0;
                //Looping through array
                for (int write = 0; write < unSortedArray.Length; write++)
                {
                    holePosition = write;
                    //If the number is first in array. The loop will not execute. 
                    //Next iteration there will be numbers  which makes the sub list.
                    //The sublist gets sorted first. 
                    while (holePosition > 0)
                    {
                        if (unSortedArray[holePosition - 1] > unSortedArray[holePosition])
                        {
                            int temp = unSortedArray[holePosition - 1];
                            unSortedArray[holePosition - 1] = unSortedArray[holePosition];
                            unSortedArray[holePosition] = temp;
                        }
                        else
                            break;
                        holePosition--;
                    }
					write.PrintIteration(unSortedArray);
                }            
				"InsertionSort Final".PrintOutput(unSortedArray);            
                return unSortedArray;
            }
            catch (Exception ex)
            {
				ex.PrintException();
                throw;
            }
        }
    }
}
