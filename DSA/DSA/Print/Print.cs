﻿using System;
namespace DSA.Print
{
    public static class Print
    {
        public static void PrintArray(this int[] outputArray)
		{
			foreach (var number in outputArray)
            {
                Console.Write(" "+number);
            }
			Console.WriteLine();
		}

		public static void PrintType(this string outputType)
        {
			Console.Write(outputType);
        }
        
		public static void PrintIteration(this int interation, int[] output)
		{
			("Iteration:" + interation).PrintType();
			output.PrintArray();
		}

		public static void PrintOutput(this string type, int[] output)
        {
			type.PrintType();
            output.PrintArray();
        }

		public static void PrintException(this Exception ex)
        {
			Console.WriteLine("ErrorMessage:{0},StackTrace:{1}",ex.Message,ex.StackTrace);
        }
    }
}
